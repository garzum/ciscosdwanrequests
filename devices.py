import requests
import json

vManage = 'https://sandbox-sdwan-2.cisco.com'

body = {
    'j_username': 'devnetuser',
    'j_password': 'RG!_Yw919_83'
}

session = requests.session()
auth_response = session.post(f'{vManage}/j_security_check', data=body, verify=None)
devices_response = session.get(f'{vManage}/dataservice/device', verify=None)

devices_dict = json.loads(devices_response.text)

topology = {
    'vmanage': 0,
    'vsmart': 0,
    'vbond': 0,
    'vedge': 0
}

print('--------- List of devices ---------')
for device in devices_dict['data']:
    hostname = device['host-name']
    device_id = device['deviceId']
    status = device['status']
    device_type = device['device-type']
    model = device['device-model']
    print(f'{hostname}({device_id}) is a {model}, and has {status} status')
    topology[device_type] += 1

print('\n--------- Topology summary ---------')
for device_type in topology.keys():
    print(f'We have {topology[device_type]} {device_type} in our topology')
