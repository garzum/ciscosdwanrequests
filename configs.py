import requests
import json

vManage = 'https://sandbox-sdwan-2.cisco.com'

body = {
    'j_username': 'devnetuser',
    'j_password': 'RG!_Yw919_83'
}

session = requests.session()
auth_response = session.post(f'{vManage}/j_security_check', data=body, verify=None)
devices_response = session.get(f'{vManage}/dataservice/device', verify=None)

devices_dict = json.loads(devices_response.text)

configs = {}

for device in devices_dict['data']:
    hostname = device['host-name']
    device_id = device['deviceId']
    config_response = session.get(f'{vManage}/dataservice/device/config', params={'deviceId': device_id}, verify=None)

    try:
        config_response.raise_for_status()
        config = config_response.text
        configs[hostname] = config
    except requests.HTTPError as e:
        print(f'Could not get config for {hostname}')

for hostname, config in configs.items():
    print(f'{hostname} config:\n{config}')
