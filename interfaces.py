import requests
import json

vManage = 'https://sandbox-sdwan-2.cisco.com'

body = {
    'j_username': 'devnetuser',
    'j_password': 'RG!_Yw919_83'
}

session = requests.session()
auth_response = session.post(f'{vManage}/j_security_check', data=body, verify=None)
devices_response = session.get(f'{vManage}/dataservice/device', verify=None)

devices_dict = json.loads(devices_response.text)

for device in devices_dict['data']:
    hostname = device['host-name']
    device_id = device['deviceId']
    interfaces_response = session.get(f'{vManage}/dataservice/device/interface', params={'deviceId': device_id},
                                      verify=None)

    try:
        interfaces_response.raise_for_status()
        interfaces = json.loads(interfaces_response.text)['data']
        print(f'--------- Interfaces of {hostname} ---------')
        for interface in interfaces:
            print(
                f'  {interface["ifname"]} - Admin status: {interface["if-admin-status"]}, '
                f'Operational status: {interface["if-oper-status"]}')
        print('')

    except requests.HTTPError as e:
        print(f'Could not get interfaces for {hostname}')
